var express = require("express");
var bodyParser = require("body-parser");
var cors = require('cors');

var app = express();
app.use(cors());


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(express.static(__dirname+'/uploads'));
app.use(express.static(__dirname+'/images'));

var port = 3005;
var events = require("./routing/events");
app.use('/api', events);

app.listen(port, function(){
    console.log('listening on port '+port);
});