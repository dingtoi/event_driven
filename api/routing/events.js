var express = require("express");
var knex = require("../config/knex");
var router = express.Router();

router.post('/event/add', function(req, res){
    var name = req.body.name;   
    var key = req.body.key;
    var desc = req.body.desc;
    var created = knex.fn.now();
    var status = 'N';

    knex('events')
    .insert({
        name:name,
        key:key,
        desc:desc,
        created_at : created,
        status:status

    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })  
})
router.post('/event/changeStatus', function(req, res){
    var id = req.body.id;
    var status = 'Y';

    knex('events')
    .update({
        status: status,
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })

})

module.exports = router;